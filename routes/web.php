<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
/*
Route::get('/', function () {
    return view('welcome');
});
*/
/*
Route::get('/welcome', function () {
    return view('listproject');
})->name('listproject');
*/
//Route::get('/project/create', 'CommentController@send')->middleware('auth')->name('projectForm');

// get all comments
Route::get('/', 'CommentController@list')->name('commentList');
// save one comment
Route::post('/comment', 'CommentController@save')->middleware('auth')->name('commentSave');
// update one comment
Route::post('/comment/{commentId}', 'CommentController@update')->middleware('auth')->name('commentUpdate');
// delete one comment DELETE
Route::get('/comment/remove/{commentId}', 'CommentController@remove')->middleware('auth')->name('commentRemove');
// show one comment in form
Route::get('/comment/edit/{commentId}', 'CommentController@editForm')->middleware('auth')->name('commentEditForm');

// USER // PROFILE
// show the profile
Route::get('/users/profile/{userId}', 'UserController@showProfile')->middleware('auth')->name('showProfile');
// show form to edit profile
Route::get('/users/profile/edit/{userId}', 'UserController@editProfile')->middleware('auth')->name('editProfile');
Route::post('/users/profile/save/{userId}', 'UserController@saveProfile')->middleware('auth')->name('saveProfile');
// '/login' that shit is inside
Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');