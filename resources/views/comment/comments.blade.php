@extends('layouts.app')
@section('content')
	@if(Auth::check())
		<div class="ui segment">

			@if($errors->any())
				<div class="ui error message">
					<i class="close icon"></i>
					<div class="header">
						Check the errors
					</div>
					<ui class="list">
						@foreach($errors->all() as $error)
							<li>{{$error}}</li>
						@endforeach
					</ui>
				</div>
			@endif
			
			<form class="ui form" method="POST" action="{{ route('commentSave') }}">
				@csrf
				<div class="fields">
				 	<div class="twelve wide field">
						{{-- <input type="text" name="comment" placeholder="What are you thinking?"> --}}

						{{-- <label>Edit your message:</label> --}}
						<textarea name="comment" rows="2" placeholder="What are you thinking?"></textarea>
					</div>
					<div class="four wide field">
						<button class="fluid ui button primary" type='submit'>Send</button>
					</div>
				</div>
			</form>
		</div>
	@endif
	<div class="ui segment">
		<div class="ui relaed divied list">
			@forelse($comments as $comment)
				@include('comment.comment', $comment)
			@empty
				<div class="item">
					<div class="content">
						No comments yet!
					</div>
				</div>
			@endforelse
		</div>
	</div>

{{-- 
	@if(Auth::check())
		<ul>
			@foreach(Auth::user()->comments as $comment)
				@include('comment.comment', $comment)
			@endforeach
		</ul>
	@endif
--}}

@endsection