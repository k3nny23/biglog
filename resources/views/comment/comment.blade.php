<div class="item">
	@if(Auth::check())
		<div class="right floated content">
			<a class='ui button' href="{{route('commentEditForm',['commentId'=>$comment->id])}}">Edit</a>
			<a class='ui button' href="{{route('commentRemove',$comment->id)}}">Delete</a>
		</div>
	@endif
	<div class="content">
		<div class="header">{{$comment->user->name}} - {{$comment->created_at}}</div>
		<span style="white-space: pre-line">{{$comment->content}}</span>
	</div>
</div>

