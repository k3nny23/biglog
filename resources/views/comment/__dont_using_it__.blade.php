@extends('layouts.app')
@section('content')
@if ($errors->any())
   <div class="ui error message">
      <i class="close icon"></i>
      <div class="heasder">
        Opravte tyto chyby
      </div>
      <ul class="list">
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
      </ul>
    </div>
@endif
<form method="POST" action="{{route('projectSave')}}">
	{{ csrf_field() }}

	<label>Name</label><input type='text' name='name'/>
	<label>Type</label><input type='text' name='type'/>
	<label>Description</label><textarea name='description'></textarea>
	<button type='submit'>Save</button>
</form>
@endsection