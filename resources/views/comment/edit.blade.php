@extends('layouts.app')
@section('content')
	{{-- ui fluid action input --}}
	<form class="ui form" method="POST" action="{{route('commentUpdate',['commentId'=>$comment->id])}}">
		@csrf
		{{-- <input value="{{$comment->content}}" name="comment" type="text"/> --}}
		<div class="field">
			<label>Edit your message:</label>
			<textarea name="comment" rows="5">{{$comment->content}}</textarea>
		</div>
		<button class="ui button green basic" type="submit"><i class="save icon"></i>Save</button>
		<a class="ui button basic" href="{{URL::previous() }}"><i class="angle left icon"></i>Back</a>
	</form>
@endsection