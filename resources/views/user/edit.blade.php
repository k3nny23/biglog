@extends('layouts.app')
@section('content')
<div class="ui segment">
	@if($errors->any())
		<div class="ui error message">
			<i class="close icon"></i>
			<div class="header">
				Check the errors
			</div>
			<ui class="list">
				@foreach($errors->all() as $error)
					<li>{{$error}}</li>
				@endforeach
			</ui>
		</div>
	@endif
	<form class="ui form" method="POST" action="{{route('saveProfile',$user->id)}}" >
		@csrf
		<div class="field">
			<label>Name</label>
			<input type="text" name="name" value="{{$user->name}}"/>
		</div>
		<div class="field">
			<label>Email</label>
			<input type="text" value="{{$user->email}}" disabled/>
		</div>
		<div class="field">
			<label>Joined at</label>
			<input type="text" value="{{$user->created_at}}" disabled/>
		</div>
		<div class="field">
			<button class="ui button basic green">Save</button>
		</div>
	</form>
</div>
@endsection