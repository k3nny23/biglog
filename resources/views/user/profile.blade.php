@extends('layouts.app')
@section('content')
<div class="ui segment">
	<h2 class="ui center aligned  icon header">
		<i class="settings icon"></i>
		<div class="content">
			Account Settings
			<div class="sub header">Manage your account settings and set e-mail preferences.</div>
		</div>
	</h2>
	@if($message)
		<div class="ui message">
			<i class="inbox icon"></i>
			<div class="header">
				Profile update
			</div>
			<p>{{$message}}</p>
		</div>
	@endif

	<div>
		<div class="ui divided huge selection list">
			<div class="item">
				<div class="ui horizontal label">Name:</div> {{$user->name}}
			</div>
			<div class="item huge">
				<div class="ui horizontal label">Email:</div> {{$user->email}}
			</div>
			<div class="item huge">
				<div class="ui horizontal label">Joined at:</div> {{$user->created_at}}
			</div>
			@if(Auth::check())
				<div class="item">
					<a class="ui basic button" href="{{route('editProfile',$user->id)}}">Edit</a>
				</div>
			@endif
			
				
			
		</div>
		
	</div>
</div>
@endsection