<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    {{-- <title>{{ config('app.name', ' WDYMeanTowit') }}</title> --}}
    <title>@yield('page.title') WDYMeanTowit</title>
    <!-- Styles -->
    <link href="{{ asset('semantic-ui/semantic.css') }}" rel="stylesheet">
</head>
<body>
    <div class="ui text container">
        <h1 class="ui header brown">
            @yield('page.title') WDYMeanTowit
        </h1>
        <div class="ui menu">
            <a class="item" href="{{ route('commentList') }}">Home</a>
            <div class="right menu">
            @guest
                <div class="item">
                    <a class="ui primary button" href="{{ route('login') }}">Login</a>
                </div>
                <div class="item">
                    <a class="ui button" href="{{ route('register') }}">Sign up for free</a>
                </div>
            @else
                <div class="item">
                    <div class="ui icon">
                        <a href="{{route('showProfile',Auth::user()->id)}}">
                            <i class="ui user outline icon"></i>
                            {{ Auth::user()->name }}
                        </a>
                    </div>
                </div>
                <div class="item">
                    <a class="ui button" href="{{ route('logout') }}" onclick="event.preventDefault();
                        document.getElementById('logout-form').submit();">Logout</a>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">@csrf
                    </form>
                </div>
            @endguest
            </div>
        </div>
        @if(session()->has('message'))
            <div class="alert alert-success">
                {{ session()->get('message') }}
            </div>
        @endif
        @yield('content')
    </div>
     
  {{--   <script src="{{ asset('js/jquery-3.3.1.min.js') }}"></script>
    <script src="{{ asset('js/semantic.min.js') }}"></script> --}}
    
    <script src="{{ asset('js/jquery-3.3.1.min.js') }}"></script>
    <script src="{{asset('semantic-ui/semantic.js')}}"></script>
    <script>
        // start the checkbox
        $('.ui.checkbox').checkbox();
        $('.message .close')
            .on('click', function() {
                $(this).closest('.message').transition('fade');
            });
    </script>
</body>
</html>
