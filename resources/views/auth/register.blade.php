
@extends('layouts.app')

@section('content')
<div class="ui segment">
    <h2 class="ui header">
        <i class="rocket icon"></i>
        <div class="content">
            Register
            <div class="sub header">You are welcome, we will be one of us!</div>
        </div>
    </h2>

<form method="POST" class="ui form" action="{{ route('register') }}">
    @csrf
    <div class="field">
        <div class="ui left icon input">
            <!-- {{ $errors->has('name') ? ' is-invalid' : '' }}-->
            <input id="name" placeholder="Type your name!" type="text" name="name" value="{{ old('name') }}" required autofocus>
            <i class="user outline icon"></i>
            @if ($errors->has('name'))
                <div class="ui error message">
                    {{ $errors->first('name') }}
                </div>
            @endif
        </div>
    </div>
    <div class="field">
        <div class="ui left icon input">
            <!-- {{ $errors->has('email') ? ' is-invalid' : '' }}" -->
            <input id="email" placeholder="abc@example.com" type="email" name="email" value="{{ old('email') }}" required>
             <i class="envelope icon"></i>
       </div>
        @if ($errors->has('email'))
            <div class="ui pointing red basic label">
                {{ $errors->first('email') }}
            </div>
        @endif

    </div>
     <div class="field">
        <div class="ui left icon input">
            <!-- {{ $errors->has('password') ? ' is-invalid' : '' }} -->
            <input id="password" placeholder="Type your password!" type="password" name="password" required>
            <i class="lock icon"></i>
        </div>
        @if ($errors->has('password'))
            <div class="ui pointing red basic label">
                {{ $errors->first('password') }}
            </div>
        @endif
    </div>
    <div class="field">
        <div class="ui left icon input">
            <input id="password-confirm" placeholder="Confirm Password" type="password" name="password_confirmation" required>
            <i class="lock icon"></i>
        </div>
    </div>
    <button type="submit" class="ui button primary">
        Register
    </button>
</form>
@endsection
