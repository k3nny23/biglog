@extends('layouts.app')
@section('content')
<div class="ui segment">
    <h1 class="ui header">Login</h1>
    <form class="ui form" method="POST" action="{{ route('login') }}">
        @csrf
        <div class="field">
            <div class="ui left icon input">
                <input id="email" type="email" name="email" value="{{ old('email') }}" placeholder="abc@example.com" required autofocus />
                <i class="user icon"></i>
                <!-- code example to put class dynamic
                    {{ $errors->has('email') ? 'is-invalid' : '' }}
                -->
                @if ($errors->has('email'))
                    <div class="ui error message">
                        <div class="header">Action Forbidden</div>
                        <p>{{ $errors->first('email') }}</p>
                    </div>
                @endif
            </div>
        </div><!-- field-->
        <div class="field">
            <div class="ui left icon input">
                <i class="lock icon"></i>
                <input id="password" type="password" name="password" required>
                <!-- code example to put class dynamic
                    {{ $errors->has('password') ? ' is-invalid' : '' }}
                -->
                @if ($errors->has('password'))
                    <div class="ui error message">
                        <div class="header">Action Forbidden</div>
                        <p>{{ $errors->first('password') }}</p>
                     </div>
                @endif
            </div>
        </div><!-- field-->
        <div class="field">
             <button type="submit" class="ui button primary">Login</button>
        </div>
        <div class="inline field">
            <div class="ui toggle checkbox">
                    <input  class="hidden" tabindex="0" type="checkbox" name="remember" {{ old('remember') ? ' checked' : '' }}/>
                    <label>Remember Me</label>
            </div>
            <div class="ui checkbox">
                <a class="btn btn-link" href="{{ route('password.request') }}">Forgot Your Password?</a>
            </div>
        </div> 
    </form>
</div>
@endsection