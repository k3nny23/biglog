@extends('layouts.app')

@section('content')

<div class="ui segment">
    <h2 class="ui header">
        <i class="handshake outline icon"></i>
        <div class="content">
            Reset Password
            <div class="sub header">Now you can reset your password!</div>
        </div>
    </h2>
    <form method="POST" class="ui form" action="{{ route('password.request') }}">
        @csrf
        <input type="hidden" name="token" value="{{ $token }}">
        <div class="field">
            <div class="ui left icon input">
                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ $email or old('email') }}" required autofocus>7
                <i class="user icon"></i>
            </div>
            @if ($errors->has('email'))
                <div class="ui pointing red basic label">
                    {{ $errors->first('email') }}
                </div>
            @endif
        </div>
        <div class="field">
            <div class="ui left icon input">
                <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>
                <i class="lock icon"></i>
           </div>
            @if ($errors->has('password'))
                 <div class="ui pointing red basic label">
                    {{ $errors->first('password') }}
                </div>
            @endif
        </div>
        <div class="field">
            <div class="ui left icon input">
                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" {{ $errors->has('password_confirmation') ? ' is-invalid' : '' }}" required>
                <i class="lock icon"></i>
            </div>
            @if ($errors->has('password_confirmation'))
                <div class="ui pointing red basic label">
                    {{ $errors->first('password_confirmation') }}
                </div>
            @endif
        </div>
        <button type="submit" class="ui button primary">
            Reset Password
        </button>
    </form>
</div>
@endsection
