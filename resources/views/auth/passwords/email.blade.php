@extends('layouts.app')

@section('content')
<div class="ui segment">
    <h2 class="ui header">
        <i class="settings icon"></i>
        <div class="content">
            Reset Password
            <div class="sub header">We will send e-mail to you recover your password!</div>
        </div>
    </h2>
    @if (session('status'))
        <div class="ui message">
            <i class="inbox icon"></i>
            <div class="content">
                <div class="header">
                    Problem detected!
                </div>
                <p>
                    {{ session('status') }}
                </p>
            </div>
        </div>
    @endif
    <form method="POST" class="ui form" action="{{ route('password.email') }}">
        @csrf
        <label for="email"></label>
        <!--{{ $errors->has('email') ? ' is-invalid' : '' }}" -->
        <div class="field">
            <div class="ui left icon input">
                <input id="email" type="email" class="form-control name="email" value="{{ old('email') }}" placeholder="abc@example.com" required>
                <i class="envelope icon"></i>
                @if ($errors->has('email'))
                <div class="ui pointing red basic label">
                    {{ $errors->first('email') }}</strong>
                </div>
                @endif
            </div>
        </div>
        <button class="ui button primary" type="submit">
            Send Password Reset Link
        </button>
    </form>
</div>
@endsection
