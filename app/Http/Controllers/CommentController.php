<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
// use Kim\Activity\Activity;
use App\User;
// use App\Post;
// use App\Changelog;
// use App\ChangelogRating;
// use App\Models\Sale;
use Illuminate\Support\Facades\Auth;
use App\Comment;

class CommentController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
    public function index()
    {
        //flash('Sorry! Please try again.')->error();
        //flash('Sorry! Please try again.')->warning();
        $post = Post::where('disabled', 0)->orderBy('id', 'DESC')->first();
        $changelog = Changelog::orderBy('created_at', 'desc')->first();
        $sale = new Sale;
        $sale = $sale->getSales();

        return view('index', ['post' => $post, 'changelog' => $changelog, 'sales' => $sale]);
    }
   */
    /**
	* List of comments!
    */
	public function list()
	{
		$comments = Comment::orderBy('id','desc')->paginate(20);
		return view('comment.comments', compact('comments')); 
	}
	/**
	* public function send(){return view('project.sendcomment')}
	*/
	public function save(Request $request)
	{
		$this->middleware('auth');
		$request->validate([
			'comment' => 'required|max:250|min:3'
		]);
		
		$comment = new Comment;
		$comment->user_id = Auth::user()->id;
		$comment->content = $request->input('comment');
		$comment->save();
		return redirect()->route('commentList')->withMessage('Comment has been created!'.$comment->id);
	}
	// option parameter
	// OPTION PARAMETER ($parameter = null) in controller
	public function editForm($commentId){
		$this->middleware('auth');
		//TO-DO check who is the own of comment!
		//$comment = Comment::where('id', $id)
		$comment = Comment::findOrFail($commentId);
		return view('comment.edit', compact('comment'));
	}
	public function update(Request $request,$commentId){
		$this->middleware('auth');
		$comment = Comment::findOrFail($commentId);
		$comment->content = $request->input('comment');
		$comment->save();
		//return view('comment.comments');
		return redirect()->route('commentList')->with('message', 'Comment '.$comment->id.' been edited!');
	}

	public function remove($id){
		$this->middleware('auth');
		//$comment = Comment::where('id', $id)
		$comment = Comment::findOrFail($id);
		$comment->delete();
		return back()->with('message', 'Comment '.$comment->id.' been delete!');
	}
}