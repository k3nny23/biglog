<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Validation\Validator;
use App\User;
use App\Http\Controllers\Controller;



class UserController extends Controller
{
    public function __construct()
	{
		$this->middleware('auth');
	}

	public function showProfile($userId){
		$user = User::findOrFail($userId);
		return view('user.profile', compact('user'))->with('message', '');
	}
	public function editProfile($userId){
		if(Auth::User()->id == $userId){
			$user = User::findOrFail($userId);
			return view('user.edit', compact('user'));
		} else {
			return redirect()->back()->withErrors(['illegal action', "You can't edit that profile!"]);
		}
	}

	public function saveProfile(Request $request, $userId){
		
		$this->middleware('auth');

		if(Auth::User()->id == $userId){

			$request->validate([
				'name' => 'required|max:250|min:3'
			]);

			$user = User::findOrFail($userId);
			$user->name = $request->input('name');
			$user->save();
			return view('user.profile', compact('user'))->with('message', 'You changed your name to '.$user->name);
		} else {
			return redirect()->back()->withErrors(['illegal action', "You can't edit that profile!"]);
		}
	}
}
